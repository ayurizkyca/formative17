package id.co.nexsoft.productmanagement.staging.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.productmanagement.staging.model.ProductClientA;

@Repository
public interface ProductClientARepository extends JpaRepository<ProductClientA, Integer> {
}
