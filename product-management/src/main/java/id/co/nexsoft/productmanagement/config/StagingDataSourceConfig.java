package id.co.nexsoft.productmanagement.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import jakarta.persistence.EntityManagerFactory;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
  entityManagerFactoryRef = "stagingEntityManagerFactory",
  transactionManagerRef = "stagingTransactionManager",
  basePackages = { "id.co.nexsoft.productmanagement.staging.repository" }
)
public class StagingDataSourceConfig {
	
	@Bean(name="stagingDataSource")
	@Primary
	@ConfigurationProperties(prefix="spring.staging.datasource")
	public DataSource accountDataSource() {
	    return DataSourceBuilder.create().build();
	}
	
	@Primary
	@Bean(name = "stagingEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean stagingEntityManagerFactory(EntityManagerFactoryBuilder builder,
			@Qualifier("stagingDataSource") DataSource stagingDataSource) {
		return builder
				.dataSource(stagingDataSource)
				.packages("id.co.nexsoft.productmanagement.staging.model")
				.build();
	}
	
	@Bean(name = "stagingTransactionManager")
	public PlatformTransactionManager stagingTransactionManager(
			@Qualifier("stagingEntityManagerFactory") EntityManagerFactory stagingEntityManagerFactory) {
		return new JpaTransactionManager(stagingEntityManagerFactory);
	}

}
