package id.co.nexsoft.productmanagement.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import jakarta.persistence.EntityManagerFactory;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
  entityManagerFactoryRef = "productionEntityManagerFactory",
  transactionManagerRef = "productionTransactionManager",
  basePackages = { "id.co.nexsoft.productmanagement.production.repository" }
)
public class ProductionDataSourceConfig {
	
	@Bean(name="productionDataSource")
	@ConfigurationProperties(prefix="spring.production.datasource")
	public DataSource accountDataSource() {
	    return DataSourceBuilder.create().build();
	}

	@Bean(name = "productionEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean productionEntityManagerFactory(EntityManagerFactoryBuilder builder,
			@Qualifier("productionDataSource") DataSource productionDataSource) {
		return builder
				.dataSource(productionDataSource)
				.packages("id.co.nexsoft.productmanagement.production.model")
				.build();
	}
	
	@Bean(name = "productionTransactionManager")
	public PlatformTransactionManager productionTransactionManager(
			@Qualifier("productionEntityManagerFactory") EntityManagerFactory productionEntityManagerFactory) {
		return new JpaTransactionManager(productionEntityManagerFactory);
	}

}
