package id.co.nexsoft.productmanagement.staging.controller;

import id.co.nexsoft.productmanagement.staging.model.ProductClientB;
import id.co.nexsoft.productmanagement.staging.service.ProductClientBService;
import jakarta.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Table(name = "product_client_b")
@RequestMapping("/products/clientB")
public class ProductClientBController {

    @Autowired
    private ProductClientBService productClientBService;

    @GetMapping
    public List<ProductClientB> getAllProducts() {
        return productClientBService.getAllProducts();
    }

    @GetMapping("/{id}")
    public ProductClientB getProductById(@PathVariable int id) {
        return productClientBService.getProductById(id);
    }

    @PostMapping
    public ProductClientB createProduct(@RequestBody ProductClientB product) {
        return productClientBService.saveProduct(product);
    }

    @PutMapping("/{id}")
    public ProductClientB updateProduct(@PathVariable int id, @RequestBody ProductClientB product) {
        return productClientBService.updateProduct(id, product);
    }

    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable int id) {
        productClientBService.deleteProduct(id);
    }
}
