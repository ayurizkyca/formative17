package id.co.nexsoft.productmanagement.staging.service;

import java.util.List;

import id.co.nexsoft.productmanagement.staging.model.ProductClientA;

public interface ProductClientAService {
    List<ProductClientA> getAllProducts();
    ProductClientA getProductById(int id);
    ProductClientA saveProduct(ProductClientA product);
    ProductClientA updateProduct(int id, ProductClientA product);
    void deleteProduct(int id);
}
