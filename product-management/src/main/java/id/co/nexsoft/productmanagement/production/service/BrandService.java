package id.co.nexsoft.productmanagement.production.service;

import java.util.List;

import id.co.nexsoft.productmanagement.production.model.Brand;

public interface BrandService {
    List<Brand> getAllBrands();
    Brand getBrandById(Integer id);
    Brand createBrand(Brand brand);
    Brand updateBrand(Integer id, Brand brand);
    void deleteBrand(Integer id);
}
