package id.co.nexsoft.productmanagement.production.service;

import java.util.List;

import id.co.nexsoft.productmanagement.production.model.Product;

public interface ProductService {
    List<Product> getAllProducts();
    Product getProductById(Integer id);
    Product saveProduct(Product product);
    Product updateProduct(Integer id, Product product);
    void deleteProduct(Integer id);
}
