package id.co.nexsoft.productmanagement.staging.controller;

import id.co.nexsoft.productmanagement.staging.model.ProductClientA;
import id.co.nexsoft.productmanagement.staging.service.ProductClientAService;
import jakarta.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Table(name = "product_client_a")
@RequestMapping("/products/clientA")
public class ProductClientAController {

    @Autowired
    private ProductClientAService productClientAService;

    @GetMapping
    public List<ProductClientA> getAllProducts() {
        return productClientAService.getAllProducts();
    }

    @GetMapping("/{id}")
    public ProductClientA getProductById(@PathVariable int id) {
        return productClientAService.getProductById(id);
    }

    @PostMapping
    public ProductClientA createProduct(@RequestBody ProductClientA product) {
        return productClientAService.saveProduct(product);
    }

    @PutMapping("/{id}")
    public ProductClientA updateProduct(@PathVariable int id, @RequestBody ProductClientA product) {
        return productClientAService.updateProduct(id, product);
    }

    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable int id) {
        productClientAService.deleteProduct(id);
    }
}
