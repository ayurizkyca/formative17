package id.co.nexsoft.productmanagement.staging.service.impl;

import id.co.nexsoft.productmanagement.staging.model.ProductClientA;
import id.co.nexsoft.productmanagement.staging.repository.ProductClientARepository;
import id.co.nexsoft.productmanagement.staging.service.ProductClientAService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductClientAServiceImpl implements ProductClientAService {

    @Autowired
    private ProductClientARepository productClientARepository;

    @Override
    public List<ProductClientA> getAllProducts() {
        return productClientARepository.findAll();
    }

    @Override
    public ProductClientA getProductById(int id) {
        Optional<ProductClientA> optionalProduct = productClientARepository.findById(id);
        return optionalProduct.orElse(null);
    }

    @Override
    public ProductClientA saveProduct(ProductClientA product) {
        return productClientARepository.save(product);
    }

    @Override
    public ProductClientA updateProduct(int id, ProductClientA product) {
        product.setId(id);
        return productClientARepository.save(product);
    }

    @Override
    public void deleteProduct(int id) {
        productClientARepository.deleteById(id);
    }
}
