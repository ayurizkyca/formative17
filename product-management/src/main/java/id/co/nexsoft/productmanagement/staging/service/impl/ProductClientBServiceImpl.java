package id.co.nexsoft.productmanagement.staging.service.impl;

import id.co.nexsoft.productmanagement.staging.model.ProductClientB;
import id.co.nexsoft.productmanagement.staging.repository.ProductClientBRepository;
import id.co.nexsoft.productmanagement.staging.service.ProductClientBService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductClientBServiceImpl implements ProductClientBService {

    @Autowired
    private ProductClientBRepository productClientBRepository;

    @Override
    public List<ProductClientB> getAllProducts() {
        return productClientBRepository.findAll();
    }

    @Override
    public ProductClientB getProductById(int id) {
        Optional<ProductClientB> optionalProduct = productClientBRepository.findById(id);
        return optionalProduct.orElse(null);
    }

    @Override
    public ProductClientB saveProduct(ProductClientB product) {
        return productClientBRepository.save(product);
    }

    @Override
    public ProductClientB updateProduct(int id, ProductClientB product) {
        product.setId(id);
        return productClientBRepository.save(product);
    }

    @Override
    public void deleteProduct(int id) {
        productClientBRepository.deleteById(id);
    }
}
