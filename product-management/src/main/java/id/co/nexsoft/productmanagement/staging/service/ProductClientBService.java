package id.co.nexsoft.productmanagement.staging.service;

import java.util.List;

import id.co.nexsoft.productmanagement.staging.model.ProductClientB;

public interface ProductClientBService {
    List<ProductClientB> getAllProducts();
    ProductClientB getProductById(int id);
    ProductClientB saveProduct(ProductClientB product);
    ProductClientB updateProduct(int id, ProductClientB product);
    void deleteProduct(int id);
}
