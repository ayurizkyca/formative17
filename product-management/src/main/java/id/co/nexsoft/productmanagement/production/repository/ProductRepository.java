package id.co.nexsoft.productmanagement.production.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.productmanagement.production.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
}
