package id.co.nexsoft.productmanagement.staging.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

@Data
@Entity
public class ProductClientB {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String nama;
    private String deskripsi;
    private String nomerArtikel;
    private double harga;
    private int persediaanBarang;
    private boolean dihapus;
    private String namaMerek;
}
