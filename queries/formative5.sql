-- mengubah field brand di table product database production
ALTER TABLE product
CHANGE COLUMN brand brand_id INT NOT NULL;


-- membuat table brand

CREATE TABLE brand(
    id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    name VARCHAR (100) NOT NULL
);