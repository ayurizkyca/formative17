CREATE TABLE product_client_a(
    id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    name VARCHAR (50) NOT NULL,
    description VARCHAR (255),
    art_number VARCHAR (20),
    price DECIMAL(20,2),
    stock INT NOT NULL,
    deleted VARCHAR (255),
    brand_name VARCHAR (100)
);


CREATE TABLE product_client_b(
    id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nama VARCHAR (50) NOT NULL,
    deskripsi VARCHAR (255),
    nomer_artikel VARCHAR (20),
    harga DECIMAL (20,2),
    persediaan_barang INT NOT NULL,
    dihapus VARCHAR (255),
    nama_merek VARCHAR (100)
);

-- mengubah tipe data field deleted
ALTER TABLE product_client_a
MODIFY COLUMN deleted TINYINT;

CREATE TABLE product(
    id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    name VARCHAR (50) NOT NULL,
    description VARCHAR (255),
    art_number VARCHAR (20),
    price DECIMAL (20,2),
    stock INT NOT NULL,
    is_deleted TINYINT (1),
    brand VARCHAR (100)
);

-- mengubah tipe data field dihapus
ALTER TABLE product_client_b 
MODIFY COLUMN dihapus TINYINT;