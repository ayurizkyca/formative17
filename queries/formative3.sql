INSERT INTO product_client_a (name, description, art_number, price, stock, deleted, brand_name) VALUES
('Smartphone Samsung Galaxy S21', 'Powerful smartphone with advanced features', 'S21-001', 12000000, 1000, 0, 'Samsung'),
('Laptop Dell XPS 13', 'Sleek and powerful laptop for professionals', 'XPS13-002', 20000000, 500, 0, 'Dell'),
('Smart TV LG OLED C1', 'High-quality OLED TV with stunning picture quality', 'OLED-C1-003', 25000000, 300, 0, 'LG'),
('Camera Sony Alpha A7 III', 'Professional mirrorless camera with outstanding performance', 'A7III-004', 15000000, 800, 0, 'Sony'),
('Smartwatch Apple Watch Series 6', 'Advanced smartwatch with health and fitness tracking', 'Watch-S6-005', 8000000, 1200, 0, 'Apple'),
('Headphones Bose QuietComfort 45', 'Premium noise-canceling headphones for immersive audio experience', 'QC45-006', 3000000, 1500, 0, 'Bose'),
('Tablet Microsoft Surface Pro 8', 'Versatile tablet with laptop performance', 'SurfacePro8-007', 18000000, 700, 0, 'Microsoft'),
('Gaming Console PlayStation 5', 'Next-gen gaming console for immersive gaming experiences', 'PS5-008', 7000000, 2000, 0, 'Sony'),
('Wireless Speaker Sonos Move', 'Portable and durable wireless speaker with great sound quality', 'Move-009', 5000000, 1000, 0, 'Sonos'),
('Router ASUS ROG Rapture GT-AX11000', 'High-performance gaming router with ultra-fast Wi-Fi speeds', 'GT-AX11000-010', 3500000, 800, 0, 'ASUS');



INSERT INTO product_client_b (nama, deskripsi, nomer_artikel, harga, persediaan_barang, dihapus, nama_merek) VALUES
('Meja Makan Kayu Jati', 'Meja makan elegan dengan bahan kayu jati berkualitas', 'MJT-001', 5000000, 50, 0, 'FurnitureKu'),
('Kursi Sofa Minimalis', 'Kursi sofa modern dengan desain minimalis yang nyaman', 'KS-002', 3500000, 100, 0, 'FurnitureKu'),
('Lemari Pakaian Sliding', 'Lemari pakaian praktis dengan pintu sliding untuk ruang yang lebih efisien', 'LPS-003', 8000000, 30, 0, 'FurnitureKu'),
('Kipas Angin Panasonic', 'Kipas angin berkualitas tinggi dengan teknologi inovatif', 'KAP-004', 500000, 200, 0, 'Panasonic'),
('AC Sharp Inverter 1 PK', 'AC split inverter dengan hemat energi dan dingin cepat', 'ACINV-005', 6000000, 50, 0, 'Sharp'),
('Kulkas 2 Pintu LG', 'Kulkas 2 pintu dengan teknologi cooling yang efisien dan tahan lama', 'KR-LG-006', 8000000, 40, 0, 'LG'),
('Mesin Cuci Samsung 10 Kg', 'Mesin cuci front load dengan kapasitas besar dan teknologi cuci yang canggih', 'MC-SAM-007', 7000000, 60, 0, 'Samsung'),
('Kompor Gas Rinnai 2 Tungku', 'Kompor gas dengan desain elegan dan tahan lama', 'KG-RIN-008', 1500000, 100, 0, 'Rinnai'),
('Blender Philips Daily Collection', 'Blender multifungsi dengan desain ergonomis', 'BL-PHI-009', 500000, 150, 0, 'Philips'),
('Panci Teflon Oxone', 'Panci teflon anti lengket untuk masakan sehat dan praktis', 'PT-OX-010', 300000, 200, 0, 'Oxone');

